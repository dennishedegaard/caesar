/**
 * @file caesar.c
 *
 * @brief Caesar cipher implemented in ansi c.
 *
 * The Cassar cipher implemented in ansi c, it features a simple
 * CLI.
 *
 * @author Dennis Hedegaard
 * @version 0.3
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>

/**
 * The name of the program.
 */
#define PROGRAM_NAME "caesar"

/**
 * Converts a string to a number, the number is expected to be positive.
 *
 * @param str The string to convert to a number. Must not be null.
 * @return Returns an integer, if an error occured -1 is returned.
 *
 */
static int str_to_number(const char *str);

/**
 * Checks to see if "base" starts with "str".
 *
 * @param base The base to look at.
 * @param str The string to look for at the beginning of "base".
 * @return Returns 1 if true, else 0 for false.
 */
static int strstart(const char *base, const char *str);

/**
 * Prints usage to the screen, happens if there's some bad arguments.
 */
static void usage(void);

/**
 * The specific implementation of the caesar cipher.
 * If input is NULL, input is read from stdin.
 * If output is NULL, output is read from stdout.
 *
 * @param shift The amount of shifting needed.
 */
static void _cipher(int shift);

/**
 * Runs the caesar cipher on the input and output specified.
 *
 * @param shift The amount of shifting needed.
 * @param input The file to read from as input, if NULL read from stdin.
 * @param output The file to write to as output, if NULL write to stdout.
 */
void cipher(int shift, const char *input, const char *output);

/**
 * An implementation of a parser for commandline arguments.
 *
 * @param argc The number of arguments.
 * @param argv An array of the arguments, NULL terminated.
 * @return If parsing is successful 1 is returned, else 0.
 */
static int parse(int argc, const char **argv);

/* Contains the shift value used, must be 1-255 to indicate valid argument. */
static int shift = 0;
/* The modifier, must be -1 or 1 to indicate valid argument. */
static int mod = 0;
/* A file pointer for input, if any input filename is supplied. */
static char *input = NULL;
/* A file pointer for output, if any output filename is supplied. */
static char *output = NULL;

/**
 * The main function, the application starts here.
 *
 * @param argc The number of arguments supplied.
 * @param argv An array of arguments, the array is NULL terminated.
 */
int main(int argc, const char **argv) {
	/* Parse arguments, check for error. If we encounter an error
	 * we call usage to print the syntax for the application.
	 */
	if (parse(argc, argv) == 0)
		usage();
	/* If the modifier is -1 (decrypt mode), inverse the shift. */
	if (mod == -1)
		shift *= -1;
	/* Do the caesar cipher itself. */
	_cipher(shift);
	/* Free input and/or output if heap was allocated while parsing. */
	if (input)
		free(input);
	if (output)
		free(output);
	/* Everything's done. */
	return EXIT_SUCCESS;
}

static void _cipher(int shift) {
	cipher(shift, input, output);
}

void cipher(int shift, const char *input, const char *output) {
	int r = 0;
	FILE *finput = NULL, *foutput = NULL;
	char buffer[BUFSIZ];
	/* Reopen stdin(0), if input != NULL. */
	if (input) {
		errno = 0;
		finput = freopen(input, "r", stdin);
		if (errno != 0) {
			fprintf(stderr, "input: %s: %s\n", strerror(errno), input);
			exit(1);
		}
	}
	/* Reopen stdout(1), if output != NULL. */
	if (output) {
		errno = 0;
		foutput = freopen(output, "w", stdout);
		if (errno != 0) {
			fprintf(stderr, "output: %s: %s\n", strerror(errno), output);
			exit(1);
		}
	}
	while ((r = fread(buffer, 1, BUFSIZ, stdin)) != 0) {
		char *end = buffer + r, *ptr = buffer;
		/* shift everything in the buffer. */
		while (ptr < end)
		*(ptr++) += shift;
		/* write the modified buffer to stdout. */
		fwrite(buffer, 1, r, stdout);
	}
	/* close any open file handles, for redirecting stdin and stdout. */
	if (finput)
		fclose(finput);
	if (foutput)
		fclose(foutput);
}

static int parse(int argc, const char **argv) {
	int i = 1;
	/* Iterate the arguments, makes the order of the arguments less
	 * importants.
	 */
	if (argc == 0)
		return 0;
	for (; i < argc; i++) {
		/* Checks for "-d" or "-e" at the start of a string.
		 * If we match we parse the "shift" and "mod" parameters.
		 * Even if there are 2 arguments, both arguments are parsed
		 * in the same run.*/
		if (strstart(argv[i], "-e") || strstart(argv[i], "-d")) {
			int len = 0;
			char c = '\0';
			/* If the modifier is already set, return 0. */
			if (mod != 0)
				return 0;
			/* Set the modifier based on the 2nd char in the argument. */
			c = argv[i][1];
			if (c == 'e') /* encoding modified is positive. */
				mod = 1;
			else if (c == 'd') /* decoding modifier is negative. */
				mod = -1;
			else { /* this should never ever happen. */
				fprintf(stderr,
						"This should never ever happen. c is: \"%c\"\n", c);
				return 0;
			}
			/* Get the length of the argument, to check the syntax. */
			len = strlen(argv[i]);
			/* If the number is not in the same argv. */
			if (len == 2) {
				/* If there are no more arguments, bad arguments. */
				if (i == argc - 1)
					return 0;
				else {
					/* Go to the next argument, and attempt to parse it
					 * as an int.
					 */
					i++;
					shift = str_to_number(argv[i]);
					/* Check to see if the number was parsed. */
					if (shift < 1)
						return 0;
				}
			}
			/* If the number is in the same argv. Since we check the start
			 * of the string to be at least 2 chars. */
			else {
				/* Make a pointer and move it +2, to get to the start
				 * of the number.
				 */
				const char *c = argv[i];
				c += 2;
				shift = str_to_number(c);
				/* Check the number parsed. */
				if (shift < 1)
					return 0;
			}
		}
		/* Attempt to parse "input" or "output". */
		else {
			/* Check to see if input is set, if not allocate some heap and set it*/
			if (!input) {
				input = malloc(strlen(argv[i]) + 1);
				strcpy(input, argv[i]);
			}
			/* If input is set, check to see if output is set, if not allocate
			 * some heap and set it. */
			else if (!output) {
				output = malloc(strlen(argv[i]) + 1);
				strcpy(output, argv[i]);
			}
			/* More arguments were supplied than what we're able to parse. */
			else
				return 0;
		}
	}
	/* No encrypt/decrypt argument(s) were found. */
	if (shift == 0 || mod == 0)
		return 0;
	/* All done, the arguments are now parsed successfully (hopefully). */
	return 1;
}

static int strstart(const char *base, const char *str) {
	/* Let's iterate the strings. */
	while (*str != '\0') {
		/* Check to see if the length of the base is less than str. */
		if (*base == '\0')
			return 0;
		/* Check to see if the next chars are equal. */
		if (*base != *str)
			return 0;
		/* Let's move on. */
		base++;
		str++;
	}
	/* We're done, success. */
	return 1;
}

static void usage(void) {
	/* Show the usage information. */
	fprintf(stderr, "Usage: %s (-e|-d)(1-255) [input] [output]\n", PROGRAM_NAME);
	exit(EXIT_FAILURE);
}

static int str_to_number(const char *str) {
	int num = 0;
	/* check for empty string */
	if (str == NULL || *str == '\0')
		return -1;
	/* throw away negative numbers. */
	if (*str == '-')
		return -1;
	errno = 0;
	/* convert and check for errors. */
	num = (int) strtol(str, NULL, 10);
	if (errno != 0) {
		return -1;
	}
	return num;
}
